var isFullLoad = false;
var thresholdDownlink = 2.5;
var connection = navigator.connection || navigator.mozConnection || navigator.webkitConnection;

function logConection() {
    console.log(connection);
    console.log(connection.downlink);
    console.log(connection.effectiveType);
}

function fullLoad (){
        var courses = document.getElementById('courses');
        courses.classList.add('courses-bg-img');

        var coursesList = document.getElementById('courses-list');
        coursesList.classList.add('layer-bg-img');

        var header = document.getElementById('home');
        header.classList.add('header-bg-img');

        var steps = document.getElementById('steps');
        steps.classList.add('content-block-bg-img');

        var partners = document.getElementById('partners');
        partners.classList.add('content-block-bg-img');

        var faq = document.getElementById('faq');
        faq.classList.add('faq-block-bg-img');

        isFullLoad = true;
}

function canConnectionLoad(){
    logConection();
    if ((connection.downlink > thresholdDownlink) && !isFullLoad) {
        fullLoad();
    }
}

canConnectionLoad();

connection.addEventListener('change', canConnectionLoad);